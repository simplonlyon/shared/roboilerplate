# Roboilerplate

## How To Use
1. cloner le projet
2. `npm install`
3. Brancher le robo (monte dans le robo shinji !)
4. `npm start`

## Troubleshooting
* Pour flasher le firmware si besoin : `npx nodebots-interchange  install git+https://github.com/Makeblock-official/mbot_nodebots -a uno --firmata=usb`
* Sur linux si le robo n'est pas détecté `sudo usermod -a -G dialout $USER`