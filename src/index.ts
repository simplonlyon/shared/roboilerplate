import { Board, Motor } from 'johnny-five';

const board = new Board();

board.on('ready', () => {
    const motorLeft = new Motor({
        pins: {
            pwm: 6, dir: 7
        }
    });
    motorLeft.forward(150);
    setTimeout(() => {

        motorLeft.stop();
    }, 1000)
})